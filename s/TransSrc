; Copyright 1998 Acorn Computers Ltd
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;     http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.
;

        GET     Hdr:ListOpts
        GET     Hdr:Macros
        GET     Hdr:System
        GET     Hdr:ModHand
        GET     Hdr:Services
        GET     Hdr:Proc
        GET     Hdr:FSNumbers
        GET     Hdr:NewErrors
        GET     Hdr:MsgTrans
        GET     Hdr:CPU.Arch
        GET     Hdr:ResourceFS
        GET     VersionASM

 [ :LNOT: :DEF: standalone
                GBLL    standalone
standalone      SETL    {FALSE}
 ]

        ^ &7F ;single byte tokens
TOTHER # 1
TAND # 1 ;expression binary operators
TDIV # 1
TEOR # 1
TMOD # 1
TOR # 1

TERROR # 1  ;miscellaneous words
TLINE # 1
TOFF # 1
TSTEP # 1
TSPC # 1
TTAB # 1
TELSE # 1
TTHEN # 1

TCONST # 1   ;(8D)

TOPENU # 1

TPTR # 1 ;polymorphics as functions
TPAGE # 1
TTIME # 1
TLOMEM # 1
THIMEM # 1
TABS # 1 ;expression class of unary operators
TACS # 1
TADC # 1
TASC # 1
TASN # 1
TATN # 1
TBGET # 1
TCOS # 1
TCOUNT # 1
TDEG # 1
TERL # 1
TERR # 1
TEVAL # 1
TEXP # 1
TEXT # 1
TFALSE # 1
TFN # 1
TGET # 1
TINKEY # 1
TINSTR # 1
TINT # 1
TLEN # 1
TLN # 1
TLOG # 1
TNOT # 1
TOPENI # 1
TOPENO # 1
TPI # 1
TPOINT # 1
TPOS # 1
TRAD # 1
TRND # 1
TSGN # 1
TSIN # 1
TSQR # 1
TTAN # 1
TTO # 1
TTRUE # 1
TUSR # 1
TVAL # 1
TVPOS # 1
TCHRD # 1  ;string expression class of unary operators
TGETD # 1
TINKED # 1
TLEFTD # 1
TMIDD # 1
TRIGHTD # 1
TSTRD # 1
TSTRND # 1
TEOF # 1

TESCFN # 1 ;Escape for Functions
TESCCOM # 1 ;Escape for Commands
TESCSTMT # 1 ;Escape for Statements

TWHEN # 1 ;statements
TOF # 1
TENDCA # 1
TELSE2 # 1
TENDIF # 1
TENDWH # 1

TPTR2 # 1 ;polymorphic again
TPAGE2 # 1
TTIME2 # 1
TLOMM2 # 1
THIMM2 # 1

TBEEP # 1
TBPUT # 1
TCALL # 1
TCHAIN # 1
TCLEAR # 1
TCLOSE # 1
TCLG # 1
TCLS # 1
TDATA # 1
TDEF # 1
TDIM # 1
TDRAW # 1
TEND # 1
TENDPR # 1
TENVEL # 1
TFOR # 1
TGOSUB # 1
TGOTO # 1
TGRAPH # 1
TIF # 1
TINPUT # 1
TLET # 1
TLOCAL # 1
TMODE # 1
TMOVE # 1
TNEXT # 1
TON # 1
TVDU # 1
TPLOT # 1
TPRINT # 1
TPROC # 1
TREAD # 1
TREM # 1
TREPEAT # 1
TREPORT # 1
TRESTORE # 1
TRETURN # 1
TRUN # 1
TSTOP # 1
TTEXT # 1
TTRACE # 1
TUNTIL # 1
TWIDTH # 1
TOSCL # 1 ;this must be <=&FF (!)

        ^ &8E ;Two byte function tokens
TSUM # 1
TBEAT # 1
TTWOFUNCLIMIT # 0

        ^ &8E ;Two byte Statement tokens
TCASE # 1
TCIRCLE # 1
TFILL # 1
TORGIN # 1
TPSET # 1
TRECT # 1
TSWAP # 1
TWHILE # 1
TWAIT # 1
TMOUSE # 1
TQUIT # 1
TSYS # 1
TINSTALLBAD # 1 ;a silly blunder
TLIBRARY # 1
TTINT # 1
TELLIPSE # 1
TBEATS # 1
TTEMPO # 1
TVOICES # 1
TVOICE # 1
TSTEREO # 1
TOVERLAY # 1
TTWOSTMTLIMIT # 0

        ^ &8E ;Two byte Command tokens
TAPPEND # 1
TAUTO # 1
TCRUNCH # 1
TDELET # 1
TEDIT # 1
THELP # 1
TLIST # 1
TLOAD # 1
TLVAR # 1
TNEW # 1
TOLD # 1
TRENUM # 1
TSAVE # 1
TTEXTLOAD # 1
TTEXTSAVE # 1
TTWIN # 1
TTWINO # 1
TINSTALL # 1
TTWOCOMMLIMIT # 0

        AREA    |BASICTrans$$Code|, CODE, READONLY, PIC

Module_BaseAddr                 *       .

BASICTrans_ModuleStart
        DCD     0 ;no MAIN code
        DCD     BASICTrans_Init-Module_BaseAddr
        DCD     BASICTrans_Die-Module_BaseAddr
      [ standalone
        DCD     BASICTrans_Service-Module_BaseAddr
      |
        DCD     0
      ]
        DCD     BASICTrans_Title-Module_BaseAddr
        DCD     BASICTrans_HelpStr-Module_BaseAddr
        DCD     0 ;BASICTrans_Commands
        DCD     BASICTransSWI_Base
        DCD     BASICTrans_SWI-Module_BaseAddr
        DCD     BASICTrans_SWINameTable-Module_BaseAddr
        DCD     0 ;
        DCD     0 ; No messages file
      [ :LNOT:No32bitCode
        DCD     BASICTrans_Flags-Module_BaseAddr
      ]

BASICTrans_Title
        DCB     "BASICTrans",0
BASICTrans_HelpStr
        DCB     "BASICTrans",9,Module_MajorVersion
        DCB     " ($Module_Date) International",0
BASICTrans_SWINameTable
        DCB     "BASICTrans",0
        DCB     "HELP",0
        DCB     "Error",0
        DCB     "Message",0
        DCB     0

message_filename
        DCB     "Resources:$.Resources.BASIC.Messages", 0
        ALIGN

      [ :LNOT:No32bitCode
BASICTrans_Flags
        DCD     ModuleFlag_32bit
      ]

BASICTrans_Init
        Push    "r1-r3, lr"
        MOV     r0, #ModHandReason_Claim
        MOV     r3, #16
        SWI     XOS_Module
      [ standalone
        BVS     %FT10
        ADRL    R0,resourcefsfiles
        SWI     XResourceFS_RegisterFiles ; ignore errors
10
      ]
        STRVC   r2, [r12]
        MOVVC   r0, r2
        ADRVC   r1, message_filename
        MOVVC   r2, #0
        SWIVC   XMessageTrans_OpenFile
        Pull    "r1-r3, pc"

BASICTrans_Die
        Push    "r0, lr"
        LDR     r0, [r12]
        CMP     r0, #0
        SWINE   XMessageTrans_CloseFile
      [ standalone
        ADRL    R0,resourcefsfiles
        SWI     XResourceFS_DeregisterFiles ; ignore errors
      ]
        CLRV
        Pull    "r0, pc"

BASICTrans_SWI
    [ :LNOT: No26bitCode
        STR     R14,[SP,#-4]!
        BL      BASICTrans_SWI_26
        LDR     R14,[SP],#4
      [ :LNOT: No32bitCode
        TEQ     PC,PC
        MOVEQ   PC,R14
      ]
        MOVVCS  PC,R14
        ORRVSS  PC,R14,#V_bit
    |
        ; Wrapper code not needed in 32bit systems
        ; Fall straight through to BASICTrans_SWI_26
    ]

BASICTrans_SWI_26
        CMP     R11,#3
        ADDCC   PC,PC,R11, LSL #2
        B       UnknownSWI
        B       BASICTrans_HELP
        B       BASICTrans_Error
        B       BASICTrans_Message
UnknownSWI
        Push    "r1-r2, lr"
        ADR     R0,UnkSWI
        LDR     r1, [r12]
        SWI     XMessageTrans_ErrorLookup
        Pull    "r1-r2, pc"
UnkSWI
        DCD     ErrorNumber_NoSuchSWI
        DCB     "BadOp",0
        ALIGN

BASICTrans_HELP
        CLRPSR  I_bit,R11
        Push    "lr"
        MOV     R1,#0
        LDRB    R10,[R0],#1
        CMP     R10,#"."
        BEQ     HELPALL1
        CMP     R10,#"["
        BEQ     HELPASS
        CMP     R10,#"@"
        BEQ     HELPAT
        BCC     HELP1
        CMP     R10,#"W"+1
        BCC     HELPALLLIMITED
        CMP     R10,#&7F
        BCS     HELPTOKEN
HELP1
        BL      message_writes
        DCB     "H0", 0
        ALIGN
        MOV     R1,#1
        B       HELPEXIT
HELPALLLIMITED
        BL      message_writes
        DCB     "H1", 0
        ALIGN
HELPALL
        LDRB    R0,[R2]
        CMP     R0,R10
        BEQ     HELPALL1
        BCS     HELPALLEND
HELPALL01
        LDRB    R0,[R2],#1
        CMP     R0,#&7F
        BCC     HELPALL01
        ADD     R2,R2,#1
        B       HELPALL
HELPALL1
        LDRB    R0,[R2],#1
        CMP     R0,#" "
        BEQ     HELPALLEND
        CMP     R10,#"."
        CMPNE   R0,R10
        BNE     HELPALLEND
        CMP     R1,#70
        MOVCS   R1,#0
        SWICS   OS_NewLine
        BCS     HELPALL12
        CMP     R1,#0
        BEQ     HELPALL12
HELPALL3
        SWI     OS_WriteI+" "
        ADD     R1,R1,#1
        CMP     R1,#10
        CMPNE   R1,#20
        CMPNE   R1,#30
        CMPNE   R1,#40
        CMPNE   R1,#50
        CMPNE   R1,#60
        CMPNE   R1,#70
        BNE     HELPALL3
HELPALL12
        SWI     OS_WriteC
        ADD     R1,R1,#1
        LDRB    R0,[R2],#1
        CMP     R0,#&7F
        BCC     HELPALL12
        ADD     R2,R2,#1
        B       HELPALL1
HELPALLEND
        CMP     R1,#0
        SWINE   OS_NewLine
        B       HELPEXITNO
HELPTOKENTABLE
        DCD     HELPTOKENINFO-HELPTOKEN
        DCD     HELPTWOFUNCTION-HELPTOKEN
        DCD     HELPTWOSTATEMENT-HELPTOKEN
        DCD     HELPTWOCOMMAND-HELPTOKEN
HELPTOKEN
        ADR     R3,HELPTOKEN
        LDR     R1,HELPTOKENTABLE
        CMP     R10,#&7F
        ADDEQ   R1,R1,R3
        BEQ     HELPTOKENSAY
        TEQ     R10,#TESCFN
        LDREQ   R1,HELPTOKENTABLE+4
        LDREQB  R10,[R0]
        TEQ     R10,#TESCSTMT
        LDREQ   R1,HELPTOKENTABLE+8
        LDREQB  R10,[R0]
        TEQ     R10,#TESCCOM
        LDREQ   R1,HELPTOKENTABLE+12
        LDREQB  R10,[R0]
        ADD     R1,R1,R3
        CMP     R10,#&7F
        BCC     HELPTOKENNOSAY
HELPTOKENFIND
        LDRB    R0,[R1],#1
        CMP     R0,#127
        BEQ     HELPTOKENNOSAY
        CMP     R0,R10
        BEQ     HELPTOKENSAY
HELPTOKENFIND1
        LDRB    r0, [r1], #1
        CMP     r0, #0
        BNE     HELPTOKENFIND1
        BEQ     HELPTOKENFIND
HELPTOKENNOSAY
        ADRL    r1, HELPTOKENNO
HELPTOKENSAY
        BL      message_write1
        SWI     OS_NewLine
        B       HELPEXITNO
HELPAT
        BL      message_writes
        DCB     "H@", 0
        ALIGN
        B       HELPEXITNO
HELPASS
        BL      message_writes
        DCB     "HASM", 0
        ALIGN
HELPEXITNO
        MOV     R1,#0 ;no help from BASIC
HELPEXIT
        CLRV
        Pull    "pc"

HELPTOKENINFO
        DCB     "HOTHERWISE",0
        DCB     TAND,"HAND",0
        DCB     TDIV,"HDIV",0
        DCB     TEOR,"HEOR",0
        DCB     TMOD,"HMOD",0
        DCB     TOR,"HOR",0
        DCB     TERROR,"HERROR",0
        DCB     TLINE,"HLINE",0
        DCB     TOFF,"HOFF",0
        DCB     TSTEP,"HSTEP", 0
        DCB     TSPC,"HSPC", 0
        DCB     TTAB,"HTAB",0
        DCB     TELSE,"HELSE",0
        DCB     TTHEN,"HTHEN",0
        DCB     TCONST,"HCONST",0
        DCB     TOPENI,"HOPENUP",0
        DCB     TABS,"HABS",0
        DCB     TACS,"HACS",0
        DCB     TADC,"HADVAL",0
        DCB     TASC,"HASC",0
        DCB     TASN,"HASN",0
        DCB     TATN,"HATN",0
        DCB     TBGET,"HBGET",0
        DCB     TCOS,"HCOS",0
        DCB     TCOUNT,"HCOUNT",0
        DCB     TDEG,"HDEG",0
        DCB     TERL,"HERL",0
        DCB     TERR,"HERR",0
        DCB     TEVAL,"HEVAL",0
        DCB     TEXP,"HEXP",0
        DCB     TEXT,"HEXT",0
        DCB     TFALSE,"HFALSE",0
        DCB     TFN,"HFN",0
        DCB     TGET,"HGET",0
        DCB     TINKEY,"HINKEY",0
        DCB     TINSTR,"HINSTR",0
        DCB     TINT,"HINT",0
        DCB     TLEN,"HLEN",0
        DCB     TLN,"HLN",0
        DCB     TLOG,"HLOG",0
        DCB     TNOT,"HNOT",0
        DCB     TOPENU,"HOPENIN",0
        DCB     TOPENO,"HOPENOUT",0
        DCB     TPI,"HPI",0
        DCB     TPOINT,"HPOINT",0
        DCB     TPOS,"HPOS",0
        DCB     TRAD,"HRAD",0
        DCB     TRND,"HRND",0
        DCB     TSGN,"HSGN",0
        DCB     TSIN,"HSIN",0
        DCB     TSQR,"HSQR",0
        DCB     TTAN,"HTAN",0
        DCB     TTO,"HTO",0
        DCB     TTRUE,"HTRUE",0
        DCB     TUSR,"HUSR",0
        DCB     TVAL,"HVAL",0
        DCB     TVPOS,"HVPOS",0
        DCB     TCHRD,"HCHR$",0
        DCB     TGETD,"HGET$",0
        DCB     TINKED,"HINKEY$",0
        DCB     TLEFTD,"HLEFT$",0
        DCB     TMIDD,"HMID$",0
        DCB     TRIGHTD,"HRIGHT$",0
        DCB     TSTRD,"HSTR$",0
        DCB     TSTRND,"HSTRING$",0
        DCB     TEOF,"HEOF",0
        DCB     TWHEN,"HWHEN",0
        DCB     TOF,"HOF",0
        DCB     TENDCA,"HENDCASE",0
        DCB     TENDIF,"HENDIF",0
        DCB     TENDWH,"HENDWHILE",0
        DCB     TPTR2,"HPTR",0
        DCB     TPAGE2,"HPAGE",0
        DCB     TTIME2,"HTIME",0
        DCB     TLOMM2,"HLOMEM",0
        DCB     THIMM2,"HHIMEM",0
        DCB     TBEEP,"HSOUND",0
        DCB     TBPUT,"HBPUT",0
        DCB     TCALL,"HCALL",0
        DCB     TCHAIN,"HCHAIN",0
        DCB     TCLEAR,"HCLEAR",0
        DCB     TCLOSE,"HCLOSE",0
        DCB     TCLG,"HCLG",0
        DCB     TCLS,"HCLS",0
        DCB     TDATA,"HDATA",0
        DCB     TDEF,"HDEF",0
        DCB     TDIM,"HDIM",0
        DCB     TDRAW,"HDRAW",0
        DCB     TEND,"HEND",0
        DCB     TENDPR,"HENDPROC",0
        DCB     TENVEL,"HENVELOPE",0
        DCB     TFOR,"HFOR",0
        DCB     TGOSUB,"HGOSUB",0
        DCB     TGOTO,"HGOTO",0
        DCB     TGRAPH, "HGCOL",0
        DCB     TIF,"HIF",0
        DCB     TINPUT,"HINPUT",0
        DCB     TLET,"HLET",0
        DCB     TLOCAL,"HLOCAL",0
        DCB     TMODE,"HMODE",0
        DCB     TMOVE,"HMOVE",0
        DCB     TNEXT,"HNEXT",0
        DCB     TON,"HON",0
        DCB     TVDU,"HVDU",0
        DCB     TPLOT,"HPLOT",0
        DCB     TPRINT,"HPRINT",0
        DCB     TPROC,"HPROC",0
        DCB     TREAD,"HREAD",0
        DCB     TREM,"HREM",0
        DCB     TREPEAT,"HREPEAT",0
        DCB     TREPORT,"HREPORT",0
        DCB     TRESTORE,"HRESTORE",0
        DCB     TRETURN,"HRETURN",0
        DCB     TRUN,"HRUN",0
        DCB     TSTOP,"HSTOP",0
        DCB     TTEXT,"HCOLOUR",0
        DCB     TTRACE,"HTRACE",0
        DCB     TUNTIL,"HUNTIL",0
        DCB     TWIDTH,"HWIDTH",0
        DCB     TOSCL,"HOSCLI",0
        DCB     TOSCL,127
HELPTWOSTATEMENT
        DCB     TCASE,"HCASE",0
        DCB     TCIRCLE,"HCIRCLE",0
        DCB     TFILL,"HFILL",0
        DCB     TORGIN,"HORIGIN",0
        DCB     TPSET,"HPOINT",0
        DCB     TRECT,"HRECTANGLE",0
        DCB     TSWAP,"HSWAP",0
        DCB     TWHILE,"HWHILE",0
        DCB     TWAIT,"HWAIT",0
        DCB     TMOUSE,"HMOUSE",0
        DCB     TQUIT,"HQUIT",0
        DCB     TSYS,"HSYS",0
        ; no help for INSTALLBAD
        DCB     TLIBRARY,"HLIBRARY",0
        DCB     TTINT,"HTINT",0
        DCB     TELLIPSE,"HELLIPSE",0
        DCB     TBEATS,"HBEATS",0
        DCB     TTEMPO,"HTEMPO",0
        DCB     TVOICES,"HVOICES",0
        DCB     TVOICE,"HVOICE",0
        DCB     TSTEREO,"HSTEREO",0
        DCB     TOVERLAY,"HOVERLAY",0
        DCB     TSYS,127
HELPTWOCOMMAND
        DCB     TAPPEND,"HAPPEND",0
        DCB     TAUTO,"HAUTO",0
        DCB     TCRUNCH,"HCRUNCH",0
        DCB     TDELET,"HDELETE",0
        DCB     TEDIT,"HEDIT",0
        DCB     THELP,"HHELP",0
        DCB     TLIST,"HLIST",0
        DCB     TLOAD,"HLOAD",0
        DCB     TLVAR,"HLVAR",0
        DCB     TNEW,"HNEW",0
        DCB     TOLD,"HOLD",0
        DCB     TRENUM,"HRENUMBER",0
        DCB     TSAVE,"HSAVE",0
        DCB     TTEXTLOAD,"HTEXTLOAD",0
        DCB     TTEXTSAVE,"HTEXTSAVE",0
        DCB     TTWIN,"HTWIN",0
        DCB     TTWINO,"HTWINO",0
        DCB     TINSTALL,"HINSTALL",0
        DCB     TSAVE,127
HELPTWOFUNCTION
        DCB     TSUM,"HSUM",0
        DCB     TBEAT,"HBEAT",0
        DCB     TSAVE,127
        ALIGN

BASICTrans_Error
        CLRPSR  I_bit,R11
        CMP     R0,#(ERRBASE-ERRTABLE):SHR:1
        BHS     UnknownSWI
        Push    "lr"
        MOV     r2, r1
        ADR     r1,ERRTABLE
      [ NoARMv4
        TST     R0,#1
        BIC     R0,R0,#1
        LDR     R1,[R1,R0,LSL #1]
        MOVEQ   R1,R1,LSL #16
        MOV     R1,R1,LSR #16
      |
        MOV     R0,R0,LSL #1
        LDRH    R1,[R1,R0]
      ]
        ADR     R3,ERRBASE
        ADD     R1,R1,R3
        LDR     r0, [r12]
        MOV     r3, #252
        SWI     XMessageTrans_Lookup
        Pull    "pc"

ERRTABLE
        DCW     EREXCEPT-ERRBASE
        DCW     ERSILL-ERRBASE
        DCW     ERNUMM-ERRBASE
        DCW     ERNUMO-ERRBASE
        DCW     ALLOCR-ERRBASE
        DCW     ERLINELONG-ERRBASE
        DCW     ERSTOP-ERRBASE
        DCW     ERLISTO-ERRBASE
        DCW     ERTWINO-ERRBASE
        DCW     ERRQ1-ERRBASE
        DCW     ONERRX-ERRBASE
        DCW     BADIC-ERRBASE
        DCW     USESLINENUMBERS-ERRBASE
        DCW     HELPTOKENNO-ERRBASE
        DCW     BADIPHEX-ERRBASE
        DCW     INSTALLBAD-ERRBASE
        DCW     ERASS1-ERRBASE
        DCW     ERASS1EQU-ERRBASE
        DCW     ERASS2-ERRBASE
        DCW     ERASS2A-ERRBASE
        DCW     ERASS2LIM-ERRBASE
        DCW     ERASS2S-ERRBASE
        DCW     ERASS3-ERRBASE
        DCW     ERASSMUL-ERRBASE
        DCW     MISSEQ-ERRBASE
        DCW     MISSEQFOR-ERRBASE
        DCW     MISTAK-ERRBASE
        DCW     ERCOMM-ERRBASE
        DCW     ERTYPEINT-ERRBASE
        DCW     ERTYPENUM-ERRBASE
        DCW     ERTYPENUMARRAY-ERRBASE
        DCW     ERTYPESTR-ERRBASE
        DCW     ERTYPESTRING-ERRBASE
        DCW     ERTYPESTRINGARRAY-ERRBASE
        DCW     ERTYPEARRAY-ERRBASE
        DCW     ERTYPEARRAYB-ERRBASE
        DCW     ERTYPEARRAYC-ERRBASE
        DCW     ERSIZE-ERRBASE
        DCW     ERTYPESWAP-ERRBASE
        DCW     ERRFN-ERRBASE
        DCW     ERDOLL-ERRBASE
        DCW     ERMISQ-ERRBASE
        DCW     ERDIMFN-ERRBASE
        DCW     ERMATMULSPACE-ERRBASE
        DCW     BADDIMSUB-ERRBASE
        DCW     BADDIMLIST-ERRBASE
        DCW     BADDIM-ERRBASE
        DCW     BADDIMSIGN-ERRBASE
        DCW     ERNDIM-ERRBASE
        DCW     BADDIMSIZE-ERRBASE
        DCW     DIMRAM-ERRBASE
        DCW     ERREND-ERRBASE
        DCW     ERRENDARRAYREF-ERRBASE
        DCW     CANTLOAD-ERRBASE
        DCW     ERRNLC-ERRBASE
        DCW     ENDPRE-ERRBASE
        DCW     ERARRW-ERRBASE
        DCW     ERARRY-ERRBASE
        DCW     ERARRYDIM-ERRBASE
        DCW     ERARRZ-ERRBASE
        DCW     ERRSUB-ERRBASE
        DCW     ERRSB2-ERRBASE
        DCW     ERSYNT-ERRBASE
        DCW     ESCAPE-ERRBASE
        DCW     ZDIVOR-ERRBASE
        DCW     ERLONG-ERRBASE
        DCW     FOVR-ERRBASE
        DCW     FOVR1-ERRBASE
        DCW     FSQRTN-ERRBASE
        DCW     ERFLOG-ERRBASE
        DCW     FRNGQQ-ERRBASE
        DCW     ERFEXP-ERRBASE
        DCW     FACERR-ERRBASE
        DCW     ERVARAR-ERRBASE
        DCW     ERBRA-ERRBASE
        DCW     ERBRA1-ERRBASE
        DCW     ERASSB1-ERRBASE
        DCW     ERASSB2-ERRBASE
        DCW     ERASSB3-ERRBASE
        DCW     ERHEX-ERRBASE
        DCW     ERHEX2-ERRBASE
        DCW     ERBIN-ERRBASE
        DCW     FNMISS-ERRBASE
        DCW     FNCALL-ERRBASE
        DCW     ARGMAT-ERRBASE
        DCW     ARGMATRET-ERRBASE
        DCW     ARGMATARR-ERRBASE
        DCW     ERNEXT-ERRBASE
        DCW     NEXTER-ERRBASE
        DCW     FORCV-ERRBASE
        DCW     FORSTEP-ERRBASE
        DCW     FORTO-ERRBASE
        DCW     ERDEEPPROC-ERRBASE
        DCW     ERGOSB-ERRBASE
        DCW     ONER-ERRBASE
        DCW     ONRGER-ERRBASE
        DCW     NOLINE-ERRBASE
        DCW     DATAOT-ERRBASE
        DCW     ERRDATASTACK-ERRBASE
        DCW     ERREPT-ERRBASE
        DCW     ERDEEPNEST-ERRBASE
        DCW     CHANNE-ERRBASE
        DCW     ERWHIL-ERRBASE
        DCW     NOENDC-ERRBASE
        DCW     ERCASE1-ERRBASE
        DCW     ERCASE-ERRBASE
        DCW     NOENDI-ERRBASE
        DCW     ERMOUS-ERRBASE
        DCW     ERSYSINPUTS-ERRBASE
        DCW     ERSYSOUTPUTS-ERRBASE
        DCW     ERINSTALL-ERRBASE
        DCW     BADPRO1-ERRBASE
        DCW     NOLIBROOM-ERRBASE
        DCW     ERASS2C-ERRBASE
        DCW     ERASSB4-ERRBASE
        DCW     ERASSFP1-ERRBASE
        DCW     ERBPP-ERRBASE
        DCW     ERMVSTK-ERRBASE
        DCW     EVFPFPIMM-ERRBASE
        DCW     EVFPSIL-ERRBASE
        DCW     EVFPSCR-ERRBASE
        DCW     EVFPSCC-ERRBASE
        DCW     EVFPRLC-ERRBASE
        DCW     EVFPRIA-ERRBASE

; Note: Limit check in BASICTrans_Error assumes ERRBASE is directly after ERRTABLE

ERRBASE
EREXCEPT = "EREXCEPT",0
ERSILL = "ERSILL",0
ERNUMM = "ERNUMM",0
ERNUMO = "ERNUMO",0
ALLOCR = "ALLOCR",0
ERLINELONG = "ERLINELONG",0
ERSTOP = "ERSTOP",0
ERLISTO = "ERLISTO",0
ERTWINO = "ERTWINO",0
ERRQ1 = "ERQ1",0
ONERRX = "ONERRX",0
BADIC = "BADIC",0
USESLINENUMBERS = "USESLINENUMBERS",0
HELPTOKENNO = "HELPTOKENNO",0
BADIPHEX = "BADIPHEX",0
INSTALLBAD = "INSTALLBAD",0
ERASS1 = "ERASS1",0
ERASS1EQU = "ERASS1EQU",0
ERASS2 = "ERASS2",0
ERASS2A = "ERASS2A",0
ERASS2LIM = "ERASS2LIM",0
ERASS2S = "ERASS2S",0
ERASS3 = "ERASS3",0
ERASSMUL = "ERASSMUL",0
MISSEQ = "MISSEQ",0
MISSEQFOR = "MISSEQFOR",0
MISTAK = "MISTAK",0
ERCOMM = "ERCOMM",0
ERTYPEINT = "ERTYPEINT",0
ERTYPENUM = "ERTYPENUM",0
ERTYPENUMARRAY = "ERTYPENUMARRAY",0
ERTYPESTR = "ERTYPESTR",0
ERTYPESTRING = "ERTYPESTRING",0
ERTYPESTRINGARRAY = "ERTYPESTRINGARRAY",0
ERTYPEARRAY = "ERTYPEARRAY",0
ERTYPEARRAYB = "ERTYPEARRAYB",0
ERTYPEARRAYC = "ERTYPEARRAYC",0
ERSIZE = "ERSIZE",0
ERTYPESWAP = "ERTYPESWAP",0
ERRFN = "ERRFN",0
ERDOLL = "ERDOLL",0
ERMISQ = "ERMISQ",0
ERDIMFN = "ERDIMFN",0
ERMATMULSPACE = "ERMATMULSPACE",0
BADDIMSUB = "BADDIMSUB",0
BADDIMLIST = "BADDIMLIST",0
BADDIM = "BADDIM",0
BADDIMSIGN = "BADDIMSIGN",0
ERNDIM = "ERNDIM",0
BADDIMSIZE = "BADDIMSIZE",0
DIMRAM = "DIMRAM",0
ERREND = "ERREND",0
ERRENDARRAYREF = "ERRENDARRAYREF",0
CANTLOAD = "CANTLOAD",0
ERRNLC = "ERRNLC",0
ENDPRE = "ENDPRE",0
ERARRW = "ERARRW",0
ERARRY = "ERARRY",0
ERARRYDIM = "ERARRYDIM",0
ERARRZ = "ERARRZ",0
ERRSUB = "ERRSUB",0
ERRSB2 = "ERRSB2",0
ERSYNT = "ERSYNT",0
ESCAPE = "ESCAPE",0
ZDIVOR = "ZDIVOR",0
ERLONG = "ERLONG",0
FOVR = "FOVR",0
FOVR1 = "FOVR1",0
FSQRTN = "FSQRTN",0
ERFLOG = "ERFLOG",0
FRNGQQ = "FRNGQQ",0
ERFEXP = "ERFEXP",0
FACERR = "FACERR",0
ERVARAR = "ERVARAR",0
ERBRA = "ERBRA",0
ERBRA1 = "ERBRA1",0
ERASSB1 = "ERASSB1",0
ERASSB2 = "ERASSB2",0
ERASSB3 = "ERASSB3",0
ERHEX = "ERHEX",0
ERHEX2 = "ERHEX2",0
ERBIN = "ERBIN",0
FNMISS = "FNMISS",0
FNCALL = "FNCALL",0
ARGMAT = "ARGMAT",0
ARGMATRET = "ARGMATRET",0
ARGMATARR = "ARGMATARR",0
ERNEXT = "ERNEXT",0
NEXTER = "NEXTER",0
FORCV = "FORCV",0
FORSTEP = "FORSTEP",0
FORTO = "FORTO",0
ERDEEPPROC = "ERDEEPPROC",0
ERGOSB = "ERGOSB",0
ONER = "ONER",0
ONRGER = "ONRGER",0
NOLINE = "NOLINE",0
DATAOT = "DATAOT",0
ERRDATASTACK = "ERRDATASTACK",0
ERREPT = "ERREPT",0
ERDEEPNEST = "ERDEEPNEST",0
CHANNE = "CHANNE",0
ERWHIL = "ERWHIL",0
NOENDC = "NOENDC",0
ERCASE1 = "ERCASE1",0
ERCASE = "ERCASE",0
NOENDI = "NOENDI",0
ERMOUS = "ERMOUS",0
ERSYSINPUTS = "ERSYSINPUTS",0
ERSYSOUTPUTS = "ERSYSOUTPUTS",0
ERINSTALL = "ERINSTALL",0
BADPRO1 = "BADPRO1",0
NOLIBROOM = "NOLIBROOM",0
ERASS2C = "ERASS2C", 0
ERASSB4 = "ERASSB4", 0
ERASSFP1 = "ERASSFP1", 0
ERBPP = "ERBPP", 0
ERMVSTK = "ERMVSTK", 0
EVFPFPIMM = "EVFPFPIMM", 0
EVFPSIL = "EVFPSIL", 0
EVFPSCR = "EVFPSCR", 0
EVFPSCC = "EVFPSCC", 0
EVFPRLC = "EVFPRLC", 0
EVFPRIA = "EVFPRIA", 0

        ALIGN

BASICTrans_Message
        CLRPSR  I_bit,R11
        CMP     r0, #26
        BCS     UnknownSWI

        Push    "r1, r2, r4-r7, lr"

        SUBS    r7, r0, #24
        CMPEQ   r1, #0
        BNE     BASICTrans_Message1 ; Don't trust things on WRCHV to preserve Z

        SWI     OS_NewLine          ; Message 24 and Line no (r1) = 0 =>
        MOV     r0, r2              ; just print <CR><Message><CR>
        SWI     OS_Write0
        SWI     OS_NewLine
        CLRV
        Pull    "r1, r2, r4-r7, pc"

BASICTrans_Message1
        SUB     sp, sp, #4 + 3 * 12

        ADD     r1, sp, #0
        MOV     r14, #'M'
        STRB    r14, [r1], #1
        MOV     r2, #4 + 3 * 12    ; Space for ALL OS_ConvertCardinals!
        SWI     OS_ConvertCardinal2

        LDR     r0, [sp, #4 + 3 * 12]
        ADD     r1, sp, #4
        SWI     OS_ConvertCardinal4
        MOV     r4, r0

        CMP     r7, #0                     ; Message 25?
        LDR     r0, [sp, #4 + 3 * 12 + 1 * 4]
        ADDNE   r1, sp, #4 + 12          ; R2 is a string pointer
        SWINE   OS_ConvertCardinal4      ; so don't convert
        MOV     r5, r0

        MOV     r0, r3
        ADD     r1, sp, #4 + 2 * 12
        SWI     OS_ConvertCardinal4
        MOV     r6, r0

        ADD     r1, sp, #0
        BL      message_write1

        ADD     sp, sp, #4 + 3 * 12

        CMP     r7, #10 - 24
        CMPNE   r7, #11 - 24
        CMPNE   r7, #16 - 24
        SWINE   OS_NewLine

        CLRV
        Pull    "r1, r2, r4-r7, pc"

message_writes
        Push    "r0-r3, lr"
      [ No26bitCode
        MOV     r1, lr          ; r1 = lr [ - PSR ]
      |
        SUB     r1, lr, pc      ; r1 = lr - pc [ - PSR ]
        ADD     r1, pc, r1      ; r1 = lr + 4 [ - PSR ]
        SUB     r1, r1, #4      ; r1 = lr [ - PSR ]
      ]
        MOV     r2, r1
10      LDRB    r0, [r2], #1
        CMP     r0, #0
        BNE     %B10
      [ No26bitCode
        ADD     r2, r2, #3
        BIC     r2, r2, #3
        STR     r2, [sp, #4 * 4]
      |
        SUB     r2, r2, r1
        ADD     r2, r2, #3
        BIC     r2, r2, #3
        ADD     lr, lr, r2
        STR     lr, [sp, #4 * 4]
      ]
        B       message_write1_tail

message_write1
        Push    "r0-r3, lr"
message_write1_tail
        LDR     r0, [r12]
        SUB     sp, sp, #2560
        MOV     r2, sp
        MOV     r3, #2560
        SWI     XMessageTrans_Lookup    ; Does dictionary substitution
        MOV     r0, r2
        SWIVC   OS_PrettyPrint          ; Substitute CR with CR/LF
        ADDS    sp, sp, #2560           ; clear V (just in case)
        Pull    "r0-r3, pc"

      [ standalone
        ALIGN
BASICTrans_ServiceTable
        DCD     0
        DCD     BASICTrans_ServiceBody - Module_BaseAddr
        DCD     Service_ResourceFSStarting      ; &60
        DCD     0

        DCD     BASICTrans_ServiceTable - Module_BaseAddr
BASICTrans_Service ROUT
        MOV     r0, r0
        TEQ     R1,#Service_ResourceFSStarting
        MOVNE   pc, lr

; ResourceFS has been reloaded - redeclare resource files
; In    R2 -> address to call
;       R3 -> workspace for ResourceFS module

BASICTrans_ServiceBody
        Push    "R0,LR"
        ADR     R0,resourcefsfiles
        MOV     LR,PC                   ; LR -> return address
        MOV     PC,R2                   ; R2 -> address to call
        Pull    "R0,PC"

resourcefsfiles
        ResourceFile    $MergedMsgs, Resources.BASIC.Messages
        DCD     0
      ]

        END
